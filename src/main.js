import Vue from 'vue'
import App from './App.vue'
//import vuetify from './plugins/vuetify';
// import { library } from '@fortawesome/fontawesome-svg-core'
// import { faUserSecret } from '@fortawesome/free-solid-svg-icons'
// import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
// import '@fortawesome/fontawesome-free/css/all.css'
// import '@fortawesome/fontawesome-free/js/all.js'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import Vuelidate from 'vuelidate'

Vue.use(VueMaterial)
Vue.use(Vuelidate)

// library.add(faUserSecret)
// Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.config.productionTip = false

new Vue({
 // vuetify,
  render: h => h(App)
}).$mount('#app')
